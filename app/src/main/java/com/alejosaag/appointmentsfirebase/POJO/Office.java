package com.alejosaag.appointmentsfirebase.POJO;

public class Office {
    private String id;
    private String description;
    private String address;
    private String phone;

    public Office() {
    }

    public Office(String id, String description, String address, String phone) {
        this.id = id;
        this.description = description;
        this.address = address;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "id=" + id + ", btn_consultorio='" + description + ", dirección=" + address + ", teléfono=" + phone;
    }
}
