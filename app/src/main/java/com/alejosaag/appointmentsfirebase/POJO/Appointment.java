package com.alejosaag.appointmentsfirebase.POJO;

public class Appointment {
    private String id;
    private Patient patient;
    private Doctor doctor;
    private Office office;
    private String appointment_date;
    private String appointment_time;

    public Appointment() {
    }

    public Appointment(Patient patient, Doctor doctor, Office office, String appointment_date, String appointment_time) {
        this.patient = patient;
        this.doctor = doctor;
        this.office = office;
        this.appointment_date = appointment_date;
        this.appointment_time = appointment_time;
    }

    public Appointment(String id, Patient patient, Doctor doctor, Office office, String appointment_date, String appointment_time) {
        this.id = id;
        this.patient = patient;
        this.doctor = doctor;
        this.office = office;
        this.appointment_date = appointment_date;
        this.appointment_time = appointment_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    @Override
    public String toString() {
        return "id => " + id + ", btn_paciente => " + patient.getFullName() + ", btn_doctor => " + doctor.getFullName() +
                ", btn_consultorio => " + office.getDescription() +
                ", fecha => " + appointment_date + ", hora => " + appointment_time;
    }
}
