package com.alejosaag.appointmentsfirebase.POJO;

public class Patient {
    private String id;
    private String identification;
    private String name;
    private String last_name;

    public Patient() {
    }

    public Patient(String id, String identification, String name, String last_name) {
        this.id = id;
        this.identification = identification;
        this.name = name;
        this.last_name = last_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFullName() {
        return this.name + " " + this.last_name;
    }

    @Override
    public String toString() {
        return "id => " + id + ", identificación => " + identification + ", nombre => " + name + " " + last_name;
    }
}
