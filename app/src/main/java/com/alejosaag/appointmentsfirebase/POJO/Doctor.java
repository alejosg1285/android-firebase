package com.alejosaag.appointmentsfirebase.POJO;

public class Doctor {
    private String id;
    private String identification;
    private String name;
    private String last_name;
    private Speciality speciality;

    public Doctor() {
    }

    public Doctor(String id, String identification, String name, String last_name, Speciality speciality) {
        this.id = id;
        this.identification = identification;
        this.name = name;
        this.last_name = last_name;
        this.speciality = speciality;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public String getFullName() {
        return this.name + " " + this.last_name;
    }

    @Override
    public String toString() {
        return "id => " + id + ", Nombre => " + name + " " + last_name + " (" + speciality.getSpeciality() + ")";
    }
}
