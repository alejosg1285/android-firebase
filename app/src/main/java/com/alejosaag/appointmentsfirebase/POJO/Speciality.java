package com.alejosaag.appointmentsfirebase.POJO;

public class Speciality {
    private String id;
    private String speciality;

    public Speciality() {
    }

    public Speciality(String id, String speciality) {
        this.id = id;
        this.speciality = speciality;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    @Override
    public String toString() {
        return "id => " + id + ", speciality => " + speciality;
    }
}
