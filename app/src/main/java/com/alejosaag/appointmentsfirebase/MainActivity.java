package com.alejosaag.appointmentsfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.alejosaag.appointmentsfirebase.Activities.AppointmentActivity;
import com.alejosaag.appointmentsfirebase.Activities.DoctorsActivity;
import com.alejosaag.appointmentsfirebase.Activities.OfficesActivity;
import com.alejosaag.appointmentsfirebase.Activities.PatientActivity;
import com.alejosaag.appointmentsfirebase.Activities.Patients.ListPatients;
import com.alejosaag.appointmentsfirebase.Activities.SpecialtyActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnPatients;
    private Button btnDoctors;
    private Button btnOffices;
    private Button btnAppointments;
    private FloatingActionButton btnFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPatients = findViewById(R.id.btnPatients);
        btnDoctors = findViewById(R.id.btnDoctors);
        btnOffices = findViewById(R.id.btnOffices);
        btnAppointments = findViewById(R.id.btnAppointments);
        btnFab = findViewById(R.id.fab);

        btnPatients.setOnClickListener(this);
        btnDoctors.setOnClickListener(this);
        btnOffices.setOnClickListener(this);
        btnAppointments.setOnClickListener(this);
        btnFab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btnDoctors:
                intent = new Intent(MainActivity.this, DoctorsActivity.class);
                break;
            case R.id.btnPatients:
                intent = new Intent(MainActivity.this, PatientActivity.class);
                break;
            case R.id.btnOffices:
                intent = new Intent(MainActivity.this, OfficesActivity.class);
                break;
            case R.id.btnAppointments:
                intent = new Intent(getApplication(), AppointmentActivity.class);
                break;
            case R.id.fab:
                intent = new Intent(MainActivity.this, SpecialtyActivity.class);
                break;
        }

        startActivity(intent);
    }
}
