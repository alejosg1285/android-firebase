package com.alejosaag.appointmentsfirebase.Activities.Doctors;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.alejosaag.appointmentsfirebase.Activities.DoctorsActivity;
import com.alejosaag.appointmentsfirebase.POJO.Doctor;
import com.alejosaag.appointmentsfirebase.POJO.Speciality;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class AddDoctor extends AppCompatActivity implements View.OnClickListener {
    private EditText txtDoctorId;
    private EditText txtDoctorIdentification;
    private EditText txtDoctorName;
    private EditText txtDoctorLastName;
    private Spinner spnSpecialities;
    private Button btnSave;
    private FloatingActionButton fab;
    private ProgressBar progressBar;
    private CoordinatorLayout coordinatorLayout;

    private FirebaseFirestore fireDb;
    private String mCollection = "doctors";
    private String mSpecialities = "mSpecialities";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_doctor);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtDoctorId = findViewById(R.id.txtDoctorId);
        txtDoctorIdentification = findViewById(R.id.txtIdentificationDoctor);
        txtDoctorName = findViewById(R.id.txtDoctorName);
        txtDoctorLastName = findViewById(R.id.txtDoctorLastName);
        spnSpecialities = findViewById(R.id.spnDoctorSpeciality);

        btnSave = findViewById(R.id.btnSaveDoctor);
        fab = findViewById(R.id.fab);
        progressBar = findViewById(R.id.pbLoading);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        progressBar.setVisibility(ProgressBar.VISIBLE);
        loadSpecialities();

        fab.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    private void loadSpecialities() {
        fireDb.collection(mSpecialities)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<Speciality> listSpecialities = new ArrayList<>();

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Speciality speciality = document.toObject(Speciality.class);

                                listSpecialities.add(speciality);
                            }

                            ArrayAdapter<Speciality> adapter = new ArrayAdapter<>(
                                    getBaseContext(), android.R.layout.simple_spinner_dropdown_item,
                                    listSpecialities
                            );
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spnSpecialities.setAdapter(adapter);

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        } else {
                            Toast.makeText(getApplication(), "No hay especialidades", Toast.LENGTH_LONG).show();

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(AddDoctor.this, DoctorsActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSaveDoctor:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                addDoctor();
            default:
                break;
        }
    }

    private void addDoctor() {
        String id = txtDoctorId.getText().toString();
        String identification = txtDoctorIdentification.getText().toString();
        String name = txtDoctorName.getText().toString();
        String lastName = txtDoctorLastName.getText().toString();
        Speciality speciality = (Speciality) spnSpecialities.getSelectedItem();

        Doctor doctor = new Doctor(id, identification, name, lastName, speciality);

        CollectionReference doctors = fireDb.collection(mCollection);

        doctors.document(id)
                .set(doctor)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.record_registed), Snackbar.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
