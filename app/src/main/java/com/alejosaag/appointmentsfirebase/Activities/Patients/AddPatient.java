package com.alejosaag.appointmentsfirebase.Activities.Patients;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AddPatient extends AppCompatActivity implements View.OnClickListener {
    FirebaseFirestore fireDb;

    private EditText txtId;
    private EditText txtIdentification;
    private EditText txtNames;
    private EditText txtLastName;
    private Button btnSave;
    private FloatingActionButton fab;

    String collection = "patients";

    private ProgressBar dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtId = findViewById(R.id.txtIdPatient);
        txtIdentification = findViewById(R.id.txtIdentificationPatient);
        txtNames = findViewById(R.id.txtNamesPatient);
        txtLastName = findViewById(R.id.txtLastNamePatient);
        btnSave = findViewById(R.id.btnSavePatient);

        dialog = findViewById(R.id.simpleProgressBar);
        btnSave.setOnClickListener(this);

        fab = findViewById(R.id.fab);
    }

    @Override
    public void onClick(final View v) {
        //dialog.setMessage(getString(R.string.wait_moment));
        //dialog.show();

        CollectionReference patients = fireDb.collection(collection);

        Map<String, Object> patient = new HashMap<>();

        patient.put("id", txtId.getText().toString());
        patient.put("identification", txtIdentification.getText().toString());
        patient.put("name", txtNames.getText().toString());
        patient.put("last_name", txtLastName.getText().toString());

        patients.document(txtIdentification.getText().toString())
                .set(patient)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplication(), getString(R.string.record_registed), Toast.LENGTH_LONG).show();
                        //Snackbar.make(v, getString(R.string.record_registed), Snackbar.LENGTH_LONG).show();
                        //dialog.dismiss();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        //dialog.dismiss();
                    }
                });
    }
}
