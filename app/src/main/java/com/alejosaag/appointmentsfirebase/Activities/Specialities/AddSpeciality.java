package com.alejosaag.appointmentsfirebase.Activities.Specialities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alejosaag.appointmentsfirebase.Activities.SpecialtyActivity;
import com.alejosaag.appointmentsfirebase.POJO.Speciality;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class AddSpeciality extends AppCompatActivity implements View.OnClickListener {
    FirebaseFirestore fireDb;

    private EditText txtSpecialityId;
    private EditText txtSpecialityName;
    private Button btnSave;
    private FloatingActionButton btnFab;
    private ProgressBar progressBar;

    private String collection = "specialities";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_speciality);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtSpecialityId = findViewById(R.id.txtIdSpeciality);
        txtSpecialityName = findViewById(R.id.txtNameSpeciality);
        btnSave = findViewById(R.id.btnSaveSpeciality);
        btnFab = findViewById(R.id.fab);
        progressBar = findViewById(R.id.pbLoading);

        btnFab.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.fab:
                intent = new Intent(AddSpeciality.this, SpecialtyActivity.class);
                startActivity(intent);
            case R.id.btnSaveSpeciality:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                addSpeciality();
                break;
            default:
                break;
        }
    }

    private void addSpeciality() {
        Speciality speciality = new Speciality(txtSpecialityId.getText().toString(), txtSpecialityName.getText().toString());

        CollectionReference specialities = fireDb.collection(collection);

        specialities.document(txtSpecialityId.getText().toString())
                .set(speciality)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplication(), getString(R.string.record_registed), Toast.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
