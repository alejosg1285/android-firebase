package com.alejosaag.appointmentsfirebase.Activities.Offices;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.alejosaag.appointmentsfirebase.Activities.OfficesActivity;
import com.alejosaag.appointmentsfirebase.POJO.Office;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class SearchOffice extends AppCompatActivity implements View.OnClickListener {
    private EditText txtOfficeId;
    private EditText txtOfficeDescription;
    private EditText txtOfficeAddress;
    private EditText txtOfficePhone;
    private Button btnSearch;
    private Button btnEdit;
    private Button btnDelete;
    private FloatingActionButton fab;
    private ProgressBar progressBar;

    private FirebaseFirestore fireDb;
    private String collection = "offices";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_office);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtOfficeId = findViewById(R.id.txtOfficeId);
        txtOfficeDescription = findViewById(R.id.txtDescriptionOffice);
        txtOfficeAddress = findViewById(R.id.txtAddressOffice);
        txtOfficePhone = findViewById(R.id.txtOfficePhone);

        btnSearch = findViewById(R.id.btnSearchOffice);
        btnEdit = findViewById(R.id.btnEditOffice);
        btnDelete = findViewById(R.id.btnDeleteOffice);
        fab = findViewById(R.id.fab);
        progressBar = findViewById(R.id.pbLoading);

        fab.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(getApplication(), OfficesActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSearchOffice:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                searchOffice();
                break;
            case R.id.btnEditOffice:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                updateOffice();
            case R.id.btnDeleteOffice:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                deleteOffice();
            default:
                break;
        }
    }

    private void searchOffice() {
        String id = txtOfficeId.getText().toString();

        DocumentReference docRef = fireDb.collection(collection).document(id);

        docRef
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();

                            if (document.exists()) {
                                Office office = document.toObject(Office.class);

                                txtOfficeDescription.setText(office.getDescription());
                                txtOfficeAddress.setText(office.getAddress());
                                txtOfficePhone.setText(office.getPhone());
                            } else {
                                txtOfficeId.setText("");
                                txtOfficeDescription.setText("");
                                txtOfficeAddress.setText("");
                                txtOfficePhone.setText("");

                                Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_exists), Snackbar.LENGTH_LONG);
                                View snackbarView = snackbar.getView();
                                snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                                snackbar.show();
                            }

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + task.getException(), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void updateOffice() {
        String id = txtOfficeId.getText().toString();
        String description = txtOfficeDescription.getText().toString();
        String address = txtOfficeAddress.getText().toString();
        String phone = txtOfficePhone.getText().toString();

        Map<String, Object> office = new HashMap<>();
        office.put("id", id);
        office.put("description", description);
        office.put("address", address);
        office.put("phone", phone);

        CollectionReference offices = fireDb.collection(collection);

        offices.document(id)
                .update(office)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.record_updated), Snackbar.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void deleteOffice() {
        String id = txtOfficeId.getText().toString();

        fireDb.collection(collection).document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.record_deleted), Snackbar.LENGTH_LONG).show();

                        txtOfficeId.setText("");
                        txtOfficeDescription.setText("");
                        txtOfficeAddress.setText("");
                        txtOfficePhone.setText("");

                        progressBar.setVisibility(ProgressBar.INVISIBLE);

                        Intent intent = new Intent(getApplication(), OfficesActivity.class);
                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
