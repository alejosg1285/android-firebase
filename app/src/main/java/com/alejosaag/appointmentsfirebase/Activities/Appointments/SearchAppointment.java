package com.alejosaag.appointmentsfirebase.Activities.Appointments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.alejosaag.appointmentsfirebase.Activities.AppointmentActivity;
import com.alejosaag.appointmentsfirebase.POJO.Appointment;
import com.alejosaag.appointmentsfirebase.POJO.Doctor;
import com.alejosaag.appointmentsfirebase.POJO.Office;
import com.alejosaag.appointmentsfirebase.POJO.Patient;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchAppointment extends AppCompatActivity implements View.OnClickListener {
    private Spinner spnAppointments;
    private Spinner spnDoctor;
    private Spinner spnPatient;
    private Spinner spnOffice;
    private TextView txtDate;
    private TextView txtTime;
    private ImageButton btnSetDate;
    private ImageButton btnSetTime;
    private Button btnSearch;
    private Button btnEdit;
    private Button btnDelete;
    private ProgressBar progressBar;
    private FloatingActionButton fab;

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la fecha
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);//Variables para obtener la hora hora
    final int hora = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);

    private static final String CERO = "0";
    private static final String BARRA = "/";
    private static final String DOS_PUNTOS = ":";

    private FirebaseFirestore fireDb;
    private String collection = "dates";
    private String doctorCollection = "doctors";
    private String officeCollection = "offices";
    private String patientCollection = "patients";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_appointment);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        spnAppointments = findViewById(R.id.spnAppointmentId);
        spnDoctor = findViewById(R.id.spnDoctorAppointment);
        spnOffice = findViewById(R.id.spnOfficeAppointment);
        spnPatient = findViewById(R.id.spnPatientAppointment);
        txtDate = findViewById(R.id.txtDate);
        txtTime = findViewById(R.id.txtTime);
        progressBar = findViewById(R.id.pbLoading);
        fab = findViewById(R.id.fab);
        btnSearch = findViewById(R.id.btnSearchAppointment);
        btnEdit = findViewById(R.id.btnEditAppointment);
        btnDelete = findViewById(R.id.btnDeleteAppointment);
        btnSetDate = findViewById(R.id.btnSetDate);
        btnSetTime = findViewById(R.id.btnSetTime);

        fab.setOnClickListener(this);
        btnSetDate.setOnClickListener(this);
        btnSetTime.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

        progressBar.setVisibility(ProgressBar.VISIBLE);
        loadAppointments();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSetDate:
                pickDate();
                break;
            case R.id.btnSetTime:
                pickTime();
                break;
            case R.id.fab:
                Intent intent = new Intent(getApplication(), AppointmentActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSearchAppointment:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                searchAppointment();
                break;
            case R.id.btnEditAppointment:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                updateAppointment();
                break;
            case R.id.btnDeleteAppointment:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                deleteAppointment();
            default:
                break;
        }
    }

    private void pickDate() {
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10) ? CERO + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10) ? CERO + String.valueOf(mesActual) : String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                txtDate.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);


            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        }, anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();
    }

    private void pickTime() {
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada = (hourOfDay < 10) ? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10) ? String.valueOf(CERO + minute) : String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if (hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                txtTime.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }

    private void loadAppointments() {
        fireDb.collection(collection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<String> list = new ArrayList<>();

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                list.add(document.getId());
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, list);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spnAppointments.setAdapter(adapter);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_documents), Snackbar.LENGTH_LONG);

                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();
                        }

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void loadPatients() {
        fireDb.collection(patientCollection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<Patient> listPatients = new ArrayList<>();

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Patient patient = document.toObject(Patient.class);

                                listPatients.add(patient);
                            }

                            ArrayAdapter<Patient> adapter = new ArrayAdapter<>(
                                    getBaseContext(), android.R.layout.simple_spinner_dropdown_item,
                                    listPatients
                            );
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spnPatient.setAdapter(adapter);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_documents), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();
                    }
                });
    }

    private void loadDoctors() {
        fireDb.collection(doctorCollection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<Doctor> listDoctors = new ArrayList<>();

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Doctor doctor = document.toObject(Doctor.class);

                                listDoctors.add(doctor);
                            }

                            ArrayAdapter<Doctor> adapter = new ArrayAdapter<>(
                                    getBaseContext(), android.R.layout.simple_spinner_dropdown_item,
                                    listDoctors
                            );
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spnDoctor.setAdapter(adapter);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_documents), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();
                    }
                });
    }

    private void loadOffices() {
        fireDb.collection(officeCollection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<Office> listOffices = new ArrayList<>();

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Office office = document.toObject(Office.class);

                                listOffices.add(office);
                            }

                            ArrayAdapter<Office> adapter = new ArrayAdapter<>(
                                    getBaseContext(), android.R.layout.simple_spinner_dropdown_item,
                                    listOffices
                            );
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spnOffice.setAdapter(adapter);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_documents), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();
                    }
                });
    }

    private int getIndexPatient(Spinner spinner, String id) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (((Patient) spinner.getItemAtPosition(i)).getId().equals(id)) {
                index = i;
                break;
            }
        }

        return index;
    }

    private int getIndexOffice(Spinner spinner, String id) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (((Office) spinner.getItemAtPosition(i)).getId().equals(id)) {
                index = i;
                break;
            }
        }

        return index;
    }

    private int getIndexDoctor(Spinner spinner, String id) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (((Doctor) spinner.getItemAtPosition(i)).getId().equals(id)) {
                index = i;
                break;
            }
        }

        return index;
    }

    private void searchAppointment() {
        String id = spnAppointments.getSelectedItem().toString();

        DocumentReference docRef = fireDb.collection(collection).document(id);

        docRef.get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();

                            if (document.exists()) {
                                Appointment appointment = document.toObject(Appointment.class);

                                txtDate.setText(appointment.getAppointment_date());
                                txtTime.setText(appointment.getAppointment_time());

                                loadPatients();
                                loadOffices();
                                loadDoctors();

                                spnPatient.setSelection(getIndexPatient(spnPatient, appointment.getPatient().getId()));
                                spnOffice.setSelection(getIndexOffice(spnOffice, appointment.getOffice().getId()));
                                spnDoctor.setSelection(getIndexDoctor(spnDoctor, appointment.getDoctor().getId()));
                            } else {
                                txtDate.setText("");
                                txtTime.setText("");
                                spnDoctor.setSelection(0);
                                spnOffice.setSelection(0);
                                spnPatient.setSelection(0);

                                Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_exists), Snackbar.LENGTH_LONG);
                                View snackbarView = snackbar.getView();
                                snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                                snackbar.show();
                            }

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + task.getException(), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void updateAppointment() {
        String id = spnAppointments.getSelectedItem().toString();
        Doctor doctor = (Doctor) spnDoctor.getSelectedItem();
        Office office = (Office) spnOffice.getSelectedItem();
        Patient patient = (Patient) spnPatient.getSelectedItem();
        String dateAppointment = txtDate.getText().toString();
        String timeAppointment = txtTime.getText().toString();

        Map<String, Object> appointment = new HashMap<>();
        appointment.put("id", id);
        appointment.put("appointment_date", dateAppointment);
        appointment.put("appointment_time", timeAppointment);

        Map<String, Object> mapDoctor = new HashMap<>();
        mapDoctor.put("id", doctor.getId());
        mapDoctor.put("identification", doctor.getIdentification());
        mapDoctor.put("name", doctor.getName());
        mapDoctor.put("last_name", doctor.getLast_name());
        Map<String, Object> mapSpeciality = new HashMap<>();
        mapSpeciality.put("id", doctor.getSpeciality().getId());
        mapSpeciality.put("speciality", doctor.getSpeciality().getSpeciality());
        mapDoctor.put("speciality", mapSpeciality);
        appointment.put("btn_doctor", mapDoctor);

        Map<String, Object> mapOffice = new HashMap<>();
        mapOffice.put("id", office.getId());
        mapOffice.put("description", office.getDescription());
        mapOffice.put("address", office.getAddress());
        mapOffice.put("phone", office.getPhone());
        appointment.put("office", mapOffice);

        Map<String, Object> mapPatient = new HashMap<>();
        mapPatient.put("id", patient.getId());
        mapPatient.put("identification", patient.getIdentification());
        mapPatient.put("name", patient.getName());
        mapPatient.put("last_name", patient.getLast_name());
        appointment.put("patient", mapPatient);

        CollectionReference reference = fireDb.collection(collection);

        reference.document(id)
                .update(appointment)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.record_updated), Snackbar.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void deleteAppointment() {
        String id = spnAppointments.getSelectedItem().toString();

        fireDb.collection(collection).document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.record_deleted), Snackbar.LENGTH_LONG).show();

                        spnAppointments.setSelection(0);
                        spnDoctor.setSelection(0);
                        spnOffice.setSelection(0);
                        spnPatient.setSelection(0);
                        txtDate.setText("");
                        txtTime.setText("");

                        progressBar.setVisibility(ProgressBar.INVISIBLE);

                        Intent intent = new Intent(getApplication(), AppointmentActivity.class);
                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
