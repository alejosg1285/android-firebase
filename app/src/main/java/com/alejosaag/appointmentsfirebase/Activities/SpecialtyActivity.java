package com.alejosaag.appointmentsfirebase.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.alejosaag.appointmentsfirebase.Activities.Specialities.AddSpeciality;
import com.alejosaag.appointmentsfirebase.Activities.Specialities.ListSpecialities;
import com.alejosaag.appointmentsfirebase.Activities.Specialities.SearchSpeciality;
import com.alejosaag.appointmentsfirebase.R;

public class SpecialtyActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAddSpeciality;
    private Button btnSearchSpeciality;
    private Button btnListSpeciality;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specialty);

        btnAddSpeciality = findViewById(R.id.btnAddSpecility);
        btnSearchSpeciality = findViewById(R.id.btnSearchSpeciality);
        btnListSpeciality = findViewById(R.id.btnListSpeciality);

        btnAddSpeciality.setOnClickListener(this);
        btnSearchSpeciality.setOnClickListener(this);
        btnListSpeciality.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.btnAddSpecility:
                intent = new Intent(SpecialtyActivity.this, AddSpeciality.class);
                startActivity(intent);
                break;
            case R.id.btnSearchSpeciality:
                intent = new Intent(SpecialtyActivity.this, SearchSpeciality.class);
                startActivity(intent);
            case R.id.btnListSpeciality:
                intent = new Intent(SpecialtyActivity.this, ListSpecialities.class);
                startActivity(intent);
            default:
                break;
        }
    }
}
