package com.alejosaag.appointmentsfirebase.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.alejosaag.appointmentsfirebase.Activities.Doctors.AddDoctor;
import com.alejosaag.appointmentsfirebase.Activities.Doctors.ListDoctors;
import com.alejosaag.appointmentsfirebase.Activities.Doctors.SearchDoctor;
import com.alejosaag.appointmentsfirebase.R;

public class DoctorsActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnAddDoctor;
    private Button btnSearchDoctor;
    private Button btnListDoctors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors);

        btnAddDoctor = findViewById(R.id.btnAddDoctor);
        btnSearchDoctor = findViewById(R.id.btnSearchDoctor);
        btnListDoctors = findViewById(R.id.btnListDoctors);

        btnAddDoctor.setOnClickListener(this);
        btnSearchDoctor.setOnClickListener(this);
        btnListDoctors.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.btnAddDoctor:
                intent = new Intent(DoctorsActivity.this, AddDoctor.class);
                break;
            case R.id.btnSearchDoctor:
                intent = new Intent(DoctorsActivity.this, SearchDoctor.class);
                break;
            case R.id.btnListDoctors:
                intent = new Intent(DoctorsActivity.this, ListDoctors.class);
                break;
            default:
                break;
        }

        startActivity(intent);
    }
}
