package com.alejosaag.appointmentsfirebase.Activities.Doctors;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.alejosaag.appointmentsfirebase.Activities.DoctorsActivity;
import com.alejosaag.appointmentsfirebase.POJO.Doctor;
import com.alejosaag.appointmentsfirebase.POJO.Speciality;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchDoctor extends AppCompatActivity implements View.OnClickListener {
    private EditText txtDoctorId;
    private EditText txtDoctorIdentification;
    private EditText txtDoctorName;
    private EditText txtDoctorLastName;
    private Spinner spnSpecialities;
    private Button btnSearch;
    private Button btnEdit;
    private Button btnDelete;
    private FloatingActionButton fab;
    private ProgressBar progressBar;
    private CoordinatorLayout coordinatorLayout;

    private FirebaseFirestore fireDb;
    private String collection = "doctors";
    private String specialities = "specialities";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_doctor);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtDoctorId = findViewById(R.id.txtDoctorId);
        txtDoctorIdentification = findViewById(R.id.txtIdentificationDoctor);
        txtDoctorName = findViewById(R.id.txtDoctorName);
        txtDoctorLastName = findViewById(R.id.txtDoctorLastName);
        spnSpecialities = findViewById(R.id.spnDoctorSpeciality);

        btnSearch = findViewById(R.id.btnSearchDoctor);
        btnEdit = findViewById(R.id.btnEditDoctor);
        btnDelete = findViewById(R.id.btnDeleteDoctor);
        fab = findViewById(R.id.fab);
        progressBar = findViewById(R.id.pbLoading);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        fab.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(SearchDoctor.this, DoctorsActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSearchDoctor:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                searchDoctor();
                break;
            case R.id.btnEditDoctor:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                updateDoctor();
                break;
            case R.id.btnDeleteDoctor:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                deleteDoctor();
                break;
            default:
                break;
        }
    }

    private void loadSpecialities() {
        fireDb.collection(specialities)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<Speciality> listSpecialities = new ArrayList<>();

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Speciality speciality = document.toObject(Speciality.class);

                                listSpecialities.add(speciality);
                            }

                            ArrayAdapter<Speciality> adapter = new ArrayAdapter<>(
                                    getBaseContext(), android.R.layout.simple_spinner_dropdown_item,
                                    listSpecialities
                            );
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spnSpecialities.setAdapter(adapter);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_documents), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();
                    }
                });
    }

    private int getIndex(Spinner spinner, String id) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (((Speciality) spinner.getItemAtPosition(i)).getId().equals(id)) {
                index = i;
                break;
            }
        }

        return index;
    }

    private void searchDoctor() {
        String id = txtDoctorId.getText().toString();

        DocumentReference docRef = fireDb.collection(collection).document(id);

        docRef.get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();

                            if (document.exists()) {
                                Doctor doctor = document.toObject(Doctor.class);

                                txtDoctorIdentification.setText(doctor.getIdentification());
                                txtDoctorName.setText(doctor.getName());
                                txtDoctorLastName.setText(doctor.getLast_name());

                                loadSpecialities();

                                spnSpecialities.setSelection(getIndex(spnSpecialities, doctor.getSpeciality().getId()));
                            } else {
                                txtDoctorId.setText("");
                                txtDoctorIdentification.setText("");
                                txtDoctorName.setText("");
                                txtDoctorLastName.setText("");
                                spnSpecialities.getSelectedItem();

                                Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_exists), Snackbar.LENGTH_LONG);
                                View snackbarView = snackbar.getView();
                                snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                                snackbar.show();
                            }

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + task.getException(), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void updateDoctor() {
        String id = txtDoctorId.getText().toString();
        String identification = txtDoctorIdentification.getText().toString();
        String name = txtDoctorName.getText().toString();
        String lastName = txtDoctorLastName.getText().toString();
        Speciality speciality = (Speciality) spnSpecialities.getSelectedItem();

        Map<String, Object> doctor = new HashMap<>();
        doctor.put("id", id);
        doctor.put("identification", identification);
        doctor.put("name", name);
        doctor.put("last_name", lastName);
        Map<String, Object> mapSpeciality = new HashMap<>();
        mapSpeciality.put("id", speciality.getId());
        mapSpeciality.put("speciality", speciality.getSpeciality());
        doctor.put("speciality", mapSpeciality);

        //Doctor btn_doctor = new Doctor(id, identification, name, lastName, speciality);

        CollectionReference doctors = fireDb.collection(collection);

        doctors.document(id)
                .update(doctor)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.record_updated), Snackbar.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void deleteDoctor() {
        String id = txtDoctorId.getText().toString();

        fireDb.collection(collection).document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.record_deleted), Snackbar.LENGTH_LONG).show();

                        txtDoctorId.setText("");
                        txtDoctorIdentification.setText("");
                        txtDoctorName.setText("");
                        txtDoctorLastName.setText("");
                        spnSpecialities.getSelectedItem();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);

                        Intent intent = new Intent(SearchDoctor.this, DoctorsActivity.class);
                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
