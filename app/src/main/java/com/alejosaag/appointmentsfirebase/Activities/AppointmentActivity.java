package com.alejosaag.appointmentsfirebase.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.alejosaag.appointmentsfirebase.Activities.Appointments.AddAppointment;
import com.alejosaag.appointmentsfirebase.Activities.Appointments.ListAppointments;
import com.alejosaag.appointmentsfirebase.Activities.Appointments.SearchAppointment;
import com.alejosaag.appointmentsfirebase.MainActivity;
import com.alejosaag.appointmentsfirebase.R;

public class AppointmentActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnAddAppointment;
    private Button btnSearchAppointment;
    private Button btnListAppointments;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);

        btnAddAppointment = findViewById(R.id.btnAddAppointment);
        btnSearchAppointment = findViewById(R.id.btnSearchAppointment);
        btnListAppointments = findViewById(R.id.btnListAppointments);
        fab = findViewById(R.id.fab);

        fab.setOnClickListener(this);
        btnAddAppointment.setOnClickListener(this);
        btnSearchAppointment.setOnClickListener(this);
        btnListAppointments.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.fab:
                intent = new Intent(getApplication(), MainActivity.class);
                break;
            case R.id.btnAddAppointment:
                intent = new Intent(getApplication(), AddAppointment.class);
                break;
            case R.id.btnSearchAppointment:
                intent = new Intent(getApplication(), SearchAppointment.class);
                break;
            case R.id.btnListAppointments:
                intent = new Intent(getApplication(), ListAppointments.class);
                break;
            default:
                break;
        }

        startActivity(intent);
    }
}
