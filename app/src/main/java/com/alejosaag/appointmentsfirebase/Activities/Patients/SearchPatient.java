package com.alejosaag.appointmentsfirebase.Activities.Patients;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class SearchPatient extends AppCompatActivity {
    FirebaseFirestore fireDb;

    private EditText txtId;
    private EditText txtIdentification;
    private EditText txtNames;
    private EditText txtLastName;
    private Button btnSearch;
    private Button btnEdit;
    private Button btnDelete;
    private LinearLayout linearLayout;

    String collection = "patients";

    private ProgressBar dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_patient);

        linearLayout = findViewById(R.id.optionsLayout);

        FirebaseApp.initializeApp(this);

        //dialog = new ProgressDialog(this);

        fireDb = FirebaseFirestore.getInstance();

        txtId = findViewById(R.id.txtIdPatient);
        txtIdentification = findViewById(R.id.txtIdentificationPatient);
        txtNames = findViewById(R.id.txtNamesPatient);
        txtLastName = findViewById(R.id.txtLastNamePatient);
        btnSearch = findViewById(R.id.btnSearchPatient);
        btnEdit = findViewById(R.id.btnEditPatient);
        btnDelete = findViewById(R.id.btnDeletePatient);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPatient(v);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePatient(v);
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePatient(v);
            }
        });
    }

    private void deletePatient(final View v) {
        //dialog.setMessage(getString(R.string.wait_moment));
        //dialog.show();

        fireDb.collection(collection).document(txtIdentification.getText().toString())
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplication(), getString(R.string.record_deleted), Toast.LENGTH_LONG).show();
                        Snackbar.make(linearLayout.getRootView(), getString(R.string.record_deleted), Snackbar.LENGTH_LONG).show();

                        txtId.setText("");
                        txtIdentification.setText("");
                        txtNames.setText("");
                        txtLastName.setText("");
                        //dialog.dismiss();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        Snackbar.make(linearLayout, "Error: " + e.getMessage(), Snackbar.LENGTH_LONG).show();
                        //dialog.dismiss();
                    }
                });
    }

    private void searchPatient(final View v) {
        //dialog.setMessage(getString(R.string.wait_moment));
        //dialog.show();

        DocumentReference docRef = fireDb.collection(collection).document(txtIdentification.getText().toString());
        docRef.get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot doc = task.getResult();

                            if (doc.exists()) {
                                txtId.setText(doc.getString("id"));
                                txtIdentification.setText(doc.getString("identification"));
                                txtNames.setText(doc.getString("name"));
                                txtLastName.setText(doc.getString("last_name"));
                                //dialog.dismiss();
                            } else {
                                Toast.makeText(getApplication(), getString(R.string.not_exists), Toast.LENGTH_LONG).show();
                                Snackbar snackbar = Snackbar.make(findViewById(R.id.optionsLayout), getString(R.string.not_exists), Snackbar.LENGTH_LONG);
                                View snackbarView = snackbar.getView();
                                snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                                snackbar.show();

                                txtId.setText("");
                                txtIdentification.setText("");
                                txtNames.setText("");
                                txtLastName.setText("");
                                //dialog.dismiss();
                            }
                        } else {
                            Toast.makeText(getApplication(), "Error: " + task.getException(), Toast.LENGTH_LONG).show();
                            //dialog.dismiss();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        //dialog.dismiss();
                    }
                });
    }

    private void updatePatient(final View v) {
        //dialog.setMessage(getString(R.string.wait_moment));
        //dialog.show();

        CollectionReference patients = fireDb.collection(collection);
        Map<String, Object> patient = new HashMap<>();
        patient.put("id", txtId.getText().toString());
        patient.put("identification", txtIdentification.getText().toString());
        patient.put("name", txtNames.getText().toString());
        patient.put("last_name", txtLastName.getText().toString());

        patients.document(txtIdentification.getText().toString())
                .update(patient)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(v, getString(R.string.record_updated), Snackbar.LENGTH_LONG).show();
                        //dialog.dismiss();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(v, getString(R.string.not_exists), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();
                    }
                });
    }
}
