package com.alejosaag.appointmentsfirebase.Activities.Appointments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.alejosaag.appointmentsfirebase.Activities.AppointmentActivity;
import com.alejosaag.appointmentsfirebase.POJO.Appointment;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ListAppointments extends AppCompatActivity {
    private EditText txtListAppointments;
    private FloatingActionButton fab;
    private ProgressBar progressBar;

    private FirebaseFirestore fireDb;
    private String collection = "dates";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_appointments);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtListAppointments = findViewById(R.id.txtListAppointments);
        fab = findViewById(R.id.fab);
        progressBar = findViewById(R.id.pbLoading);

        progressBar.setVisibility(ProgressBar.VISIBLE);
        appointmentList();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), AppointmentActivity.class);
                startActivity(intent);
            }
        });
    }

    private void appointmentList() {
        fireDb.collection(collection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            String text = "";
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Appointment appointment = document.toObject(Appointment.class);

                                text += appointment.toString().replace(',', '\n') + "\n";
                            }

                            txtListAppointments.setText(text);

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.not_documents), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
