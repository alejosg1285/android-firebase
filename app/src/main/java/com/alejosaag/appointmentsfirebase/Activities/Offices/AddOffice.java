package com.alejosaag.appointmentsfirebase.Activities.Offices;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.alejosaag.appointmentsfirebase.Activities.OfficesActivity;
import com.alejosaag.appointmentsfirebase.POJO.Office;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class AddOffice extends AppCompatActivity implements View.OnClickListener {
    private EditText txtOfficeId;
    private EditText txtOfficeDescription;
    private EditText txtOfficeAddress;
    private EditText txtOfficePhone;
    private Button btnSave;
    private FloatingActionButton fab;
    private ProgressBar progressBar;

    private FirebaseFirestore fireDb;
    private String collection = "offices";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_office);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtOfficeId = findViewById(R.id.txtOfficeId);
        txtOfficeDescription = findViewById(R.id.txtDescriptionOffice);
        txtOfficeAddress = findViewById(R.id.txtAddressOffice);
        txtOfficePhone = findViewById(R.id.txtOfficePhone);

        btnSave = findViewById(R.id.btnSaveOffice);
        fab = findViewById(R.id.fab);
        progressBar = findViewById(R.id.pbLoading);

        btnSave.setOnClickListener(this);
        fab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(getApplication(), OfficesActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSaveOffice:
                progressBar.setVisibility(ProgressBar.VISIBLE);

                addOffice();
                break;
            default:
                break;
        }
    }

    private void addOffice() {
        String id = txtOfficeId.getText().toString();
        String description = txtOfficeDescription.getText().toString();
        String address = txtOfficeAddress.getText().toString();
        String phone = txtOfficePhone.getText().toString();

        Office office = new Office(id, description, address, phone);

        CollectionReference offices = fireDb.collection(collection);

        offices.document(id)
                .set(office)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.record_registed), Snackbar.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Error: " + e.getMessage(), Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                        snackbar.show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
