package com.alejosaag.appointmentsfirebase.Activities.Specialities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alejosaag.appointmentsfirebase.Activities.SpecialtyActivity;
import com.alejosaag.appointmentsfirebase.POJO.Speciality;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class SearchSpeciality extends AppCompatActivity implements View.OnClickListener {
    private TextView txtSpecialityId;
    private TextView txtSpeciality;
    private Button btnSearch;
    private Button btnEdit;
    private Button btnDelete;

    private ProgressBar progressBar;
    private FloatingActionButton fab;

    private String collection = "specialities";

    FirebaseFirestore fireDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_speciality);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtSpecialityId = findViewById(R.id.txtIdSpeciality);
        txtSpeciality = findViewById(R.id.txtNameSpeciality);
        btnSearch = findViewById(R.id.btnSearchSpeciality);
        btnEdit = findViewById(R.id.btnEditSpeciality);
        btnDelete = findViewById(R.id.btnDeleteSpeciality);

        progressBar = findViewById(R.id.pbLoading);
        fab = findViewById(R.id.fab);

        btnSearch.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        fab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(SearchSpeciality.this, SpecialtyActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSearchSpeciality:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                searchSpeciality();
                break;
            case R.id.btnEditSpeciality:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                updateSpeciality();
                break;
            case R.id.btnDeleteSpeciality:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                deleteSpeciality();
                break;
            default:
                break;
        }
    }

    private void searchSpeciality() {
        String id = txtSpecialityId.getText().toString();

        DocumentReference docRef = fireDb.collection(collection).document(id);
        docRef.get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();

                            if (document.exists()) {
                                Speciality speciality = document.toObject(Speciality.class);
                                txtSpecialityId.setText(speciality.getId());
                                txtSpeciality.setText(speciality.getSpeciality());
                            } else {
                                Toast.makeText(getApplication(), getString(R.string.not_exists), Toast.LENGTH_LONG).show();

                                txtSpecialityId.setText("");
                                txtSpeciality.setText("");
                            }

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        } else {
                            Toast.makeText(getApplication(), "Error: " + task.getException(), Toast.LENGTH_LONG).show();

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void updateSpeciality() {
        String id = txtSpecialityId.getText().toString();
        String specia = txtSpeciality.getText().toString();

        Map<String, Object> speciality = new HashMap<>();
        speciality.put("id", id);
        speciality.put("speciality", specia);

        CollectionReference specialities = fireDb.collection(collection);

        specialities.document(id)
                .update(speciality)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplication(), getString(R.string.record_updated), Toast.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);

                        Intent intent = new Intent(SearchSpeciality.this, SpecialtyActivity.class);
                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }

    private void deleteSpeciality() {
        String id = txtSpecialityId.getText().toString();

        fireDb.collection(collection).document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplication(), getString(R.string.record_deleted), Toast.LENGTH_LONG).show();

                        txtSpecialityId.setText("");
                        txtSpeciality.setText("");

                        progressBar.setVisibility(ProgressBar.INVISIBLE);

                        Intent intent = new Intent(SearchSpeciality.this, SpecialtyActivity.class);
                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
