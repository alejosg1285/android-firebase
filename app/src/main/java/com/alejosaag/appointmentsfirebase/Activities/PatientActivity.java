package com.alejosaag.appointmentsfirebase.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.alejosaag.appointmentsfirebase.Activities.Patients.AddPatient;
import com.alejosaag.appointmentsfirebase.Activities.Patients.ListPatients;
import com.alejosaag.appointmentsfirebase.Activities.Patients.SearchPatient;
import com.alejosaag.appointmentsfirebase.R;

public class PatientActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnAddPatient;
    private Button btnSearchPatient;
    private Button btnListPatients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        btnAddPatient = findViewById(R.id.btnAddPatient);
        btnSearchPatient = findViewById(R.id.btnSearchPatient);
        btnListPatients = findViewById(R.id.btnListPatients);

        btnAddPatient.setOnClickListener(this);
        btnSearchPatient.setOnClickListener(this);
        btnListPatients.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btnAddPatient:
                intent = new Intent(PatientActivity.this, AddPatient.class);
                break;
            case R.id.btnSearchPatient:
                intent = new Intent(PatientActivity.this, SearchPatient.class);
                break;
            case R.id.btnListPatients:
                intent = new Intent(PatientActivity.this, ListPatients.class);
                break;
        }

        startActivity(intent);
    }
}