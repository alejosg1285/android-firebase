package com.alejosaag.appointmentsfirebase.Activities.Specialities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alejosaag.appointmentsfirebase.Activities.SpecialtyActivity;
import com.alejosaag.appointmentsfirebase.POJO.Speciality;
import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ListSpecialities extends AppCompatActivity {
    private FirebaseFirestore fireDb;
    private EditText txtList;
    private FloatingActionButton fab;

    private String collection = "specialities";

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_specialities);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        txtList = findViewById(R.id.txtListSpeciality);
        fab = findViewById(R.id.fab);

        progressBar = findViewById(R.id.pbLoading);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListSpecialities.this, SpecialtyActivity.class);
                startActivity(intent);
            }
        });

        specialityList();
    }

    private void specialityList() {
        fireDb.collection(collection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            String text = "";
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Speciality speciality = document.toObject(Speciality.class);

                                text += speciality.toString() + "\n";
                            }

                            txtList.setText(text);

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        } else {
                            Toast.makeText(getApplication(), getString(R.string.not_documents), Toast.LENGTH_LONG).show();

                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    }
                });
    }
}
