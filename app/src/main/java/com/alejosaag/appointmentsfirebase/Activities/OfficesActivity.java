package com.alejosaag.appointmentsfirebase.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.alejosaag.appointmentsfirebase.Activities.Offices.AddOffice;
import com.alejosaag.appointmentsfirebase.Activities.Offices.ListOffices;
import com.alejosaag.appointmentsfirebase.Activities.Offices.SearchOffice;
import com.alejosaag.appointmentsfirebase.MainActivity;
import com.alejosaag.appointmentsfirebase.R;

public class OfficesActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnAddOffice;
    private Button btnSearchOffice;
    private Button btnListOffices;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offices);

        btnAddOffice = findViewById(R.id.btnAddOffice);
        btnSearchOffice = findViewById(R.id.btnSearchOffice);
        btnListOffices = findViewById(R.id.btnListOffices);
        fab = findViewById(R.id.fab);

        fab.setOnClickListener(this);
        btnAddOffice.setOnClickListener(this);
        btnSearchOffice.setOnClickListener(this);
        btnListOffices.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.fab:
                intent = new Intent(getApplication(), MainActivity.class);
                break;
            case R.id.btnAddOffice:
                intent = new Intent(getApplication(), AddOffice.class);
                break;
            case R.id.btnSearchOffice:
                intent = new Intent(getApplication(), SearchOffice.class);
                break;
            case R.id.btnListOffices:
                intent = new Intent(getApplication(), ListOffices.class);
                break;
            default:
                break;
        }

        startActivity(intent);
    }
}
