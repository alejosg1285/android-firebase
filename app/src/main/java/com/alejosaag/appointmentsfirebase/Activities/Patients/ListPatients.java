package com.alejosaag.appointmentsfirebase.Activities.Patients;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alejosaag.appointmentsfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ListPatients extends AppCompatActivity {
    private FirebaseFirestore fireDb;
    EditText    txtList;
    String collection = "patients";

    private ProgressBar dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_patients);

        FirebaseApp.initializeApp(this);
        fireDb = FirebaseFirestore.getInstance();

        //dialog = new ProgressDialog(this);
        txtList = findViewById(R.id.txtListPatient);

        //dialog.setMessage(getString(R.string.wait_moment));
        //dialog.show();

        patientList();
    }

    public void patientList() {
        fireDb.collection(collection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            String text = "";
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                text += String.format("Document => %s\n", document.getId());
                                text += String.format("Id => %s\n", document.getString("id"));
                                text += String.format("Identificación => %s\n", document.getString("identification"));
                                text += String.format("Nombres => %s\n", document.getString("name"));
                                text += String.format("Apellidos => %s\n\n", document.getString("last_name"));
                            }

                            txtList.setText(text);
                            //dialog.dismiss();
                        } else {
                            Toast.makeText(getApplication(), getString(R.string.not_documents), Toast.LENGTH_LONG).show();
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.optionsLayout), getString(R.string.not_documents), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#EF3829"));
                            snackbar.show();

                            //dialog.dismiss();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        //dialog.dismiss();
                    }
                });
    }
}
